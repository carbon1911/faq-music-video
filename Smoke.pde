import java.util.function.Consumer;

color SMOKE_COLOR = color(80);

// ParticleSystem source: https://www.processing.org/examples/simpleparticlesystem.html

// A class to describe a group of Particles
// An ArrayList is used to manage the list of Particles 

class ParticleSystem implements Drawable {
	ArrayList<Particle> particles;
	PVector origin, dir;
	int partR = 0;
	float displ = 0.0;
	float lifespan = 0.0;
	boolean running = false;
	int addParticleDeltaFrames = 1;
	ArrayList<PVector> lightDislocs = new ArrayList();
	color mColor = color(SMOKE_COLOR);
	
	PGraphics offScreen = null;
	PGraphics particleLightBuffer = null;

	final TransformationContext blendModeAddTransformationContext = new TransformationContext(blendMode).withParams(ADD);
	final TransformationContext particleDrawMainCanvasTransformationContext = prepareParticleDrawTransformationContext(g_canvas);
	final TransformationContext endBeginMainCanvas = new TransformationContext(endBeginDraw, g_canvas);
	TransformationContext particleDrawOffscreenTransformationContext = null;
	TransformationContext tintTransformationContext = null;
	TransformationContext particleLightBufferTransformationContext = null;

	final Runnable drawParticles = new Runnable() { public void run() { drawParticles(); } };
	final Runnable drawLightDislocations = new Runnable()
	{
		//todo: tintTransformationContext can be a part of this

		public void run()
		{
			for (final PVector lightDisloc : lightDislocs)
			{
				tintTransformationContext.applyOn(drawLight, lightDisloc);
			}
		}
	};
	final Runnable drawOffscreenAndLightDislocations = new Runnable()
	{
		public void run()
		{
			particleDrawOffscreenTransformationContext.applyOn(offscreenDrawParticles);
			particleLightBufferTransformationContext.applyOn(drawLightDislocations);
		}
	};
	Runnable offscreenDrawParticles = null;

    final Consumer<PVector> drawLight = new Consumer<PVector>()
    {
        public void accept(final PVector lightDislocation) { drawLight(lightDislocation); }
    };

	ParticleSystem(PVector position, PVector direction, int particleRadius, float lifeSpan, float displacement) {
		origin = position.copy();
		particles = new ArrayList<Particle>();
		partR = particleRadius;
		dir = direction;
		lifespan = lifeSpan;
		displ = displacement;

		offScreen = createGraphics(1920, 1080, P2D);
		particleDrawOffscreenTransformationContext = prepareParticleDrawOffscreenTransformationContext(offScreen);
		
		particleLightBuffer = createGraphics(1920, 1080, P2D);
		particleLightBufferTransformationContext = prepareBeginEndClearTransformationContext(particleLightBuffer);

		tintTransformationContext = prepareTintTransformationContext();
        offscreenDrawParticles = new Runnable() { public void run() { drawParticles(offScreen); } };
	}

	private TransformationContext prepareParticleDrawTransformationContext(final PGraphics buffer)
	{
		final TransformationContext head = new TransformationContext(noStroke, buffer);
		head.followedBy(fill).withParams(new Supplier() 
		{
			// Apparently color is an int under the hood
			public Integer get() { return mColor; }
		});
		return head;
	}

	private TransformationContext prepareBeginEndClearTransformationContext(final PGraphics buffer)
	{
		final TransformationContext head = new TransformationContext(beginEndDraw, buffer);
		head.followedBy(clear);
		return head;
	}

	private TransformationContext prepareParticleDrawOffscreenTransformationContext(final PGraphics buffer)
	{
		final TransformationContext head = prepareBeginEndClearTransformationContext(buffer);
		final TransformationContext beginEndClearTail = head.getTail();
		beginEndClearTail.followedBy(prepareParticleDrawTransformationContext(buffer));
		return head;
	}

	private TransformationContext prepareTintTransformationContext()
	{
		final TransformationContext head = new TransformationContext(noStroke, particleLightBuffer);
		head.followedBy(tint).withParams(new Supplier() 
			{
				private final float[] data = new float[]{ 255.f, 0.f, 0.f, 0.f };
				public float[] get()
				{
					data[3] = fireTruck.siren.visibility() * fireTruck.absoluteDistanceFromStopPlaceClamped();
					return data;
				}
			}
        );
		return head;
	}

    void display()
    {
        if (running && frameCount % addParticleDeltaFrames == 0)
        {
            addParticle();
        }
        updateAll();

		endBeginMainCanvas.applyOn(drawOffscreenAndLightDislocations);
		particleDrawMainCanvasTransformationContext.applyOn(drawParticles);
    }

	private void drawParticles(final PGraphics buffer)
	{
		for (Particle p : particles)
        {
            p.display(buffer);
        }
	}

	private void drawParticles()
	{
		drawParticles(g_canvas);
        if (fireTruck.displaying)
        {
			blendModeAddTransformationContext.applyOn(particleLightBuffer);
        }
	}

	private void drawLight(final PVector lightDisloc)
	{
		particleLightBuffer.image(offScreen, 0, 0);

		final TransformationContext head = new TransformationContext(blendMode, particleLightBuffer)
			.withParams(SUBTRACT);
		head.followedBy(translate).withParams(lightDisloc);
		head.applyOn(offScreen);
	}

    void addParticle() {
        particles.add(new Particle(origin, dir, partR, lifespan, displ));
    }
    
    void updateAll() {
        for (int i = particles.size()-1; i >= 0; i--) {
            Particle p = particles.get(i);
            p.update();
            if (p.isDead()) {
                particles.remove(i);
            }
        }
    }
    
    void play()
    {
        running = true;
    }
    
    void pause()
    {
        running = false;
    }
    
    ParticleSystem setAddParticleDeltaFrames(int value)
    {
        addParticleDeltaFrames = value;
        return this;
    }
    
    ParticleSystem addLightDisloc(PVector disloc)
    {
        lightDislocs.add(disloc);
        return this;
    }
    
    ParticleSystem setColor(color newColor)
    {
        mColor = newColor;
        return this;
    }
    
    boolean isEmpty()
    {
        return particles.isEmpty();
    }
}


// A simple Particle class

class Particle {
	PVector position;
	PVector velocity;
	float lifespan;
	int partR = 0;

	Particle(PVector l, PVector direction, int particleRadius, float lifeSpan, float displacementMag) {
		PVector displ = new PVector(direction.y * random(-displacementMag, displacementMag), -direction.x * random(-displacementMag, displacementMag));
		velocity = direction.copy().add(displ);
		position = l.copy();
		lifespan = lifeSpan;
		partR = particleRadius;
	}

	// Method to update position
	void update() {
		position.add(velocity);
		lifespan -= 1.0;
	}

  // Method to display, make sure scale is 0 and translate is 0, 0
    void display(final PGraphics buffer) {
        
        // Ideally TransformationContext was meant to be created as an attribute to aviod allocations in tick,
        // but it seems that the performance is not hurt. Still, try to avoid allocations in tick. It's unavoidable here
        // because the buffer variable is resolved in tick
		new TransformationContext(ellipseMode, buffer).withParams(CENTER).applyOn(
			new Runnable(){ public void run() { buffer.ellipse(position.x, position.y, partR, partR); } }
		);
    }

  // Is the particle still useful?
	boolean isDead() {
		return lifespan < 0.0;
	}
}

class StaticSmoke implements Drawable
{
	TransformationContext smokeTransformationContext = this.prepareTransformationContext();
	Runnable drawEllipses = new Runnable()
	{
		public void run()
		{
			for (int ellipseCenter = 0; ellipseCenter < 20; ++ellipseCenter)
            {
                g_canvas.ellipse(ellipseCenter * 200, 50, 300, 300);
            }
		}
	};

	private TransformationContext prepareTransformationContext()
	{
		TransformationContext head = new TransformationContext(ellipseMode).withParams(CENTER);
		head.followedBy(new TransformationContext(noStroke))
			.followedBy(new TransformationContext(fill)).withParams(SMOKE_COLOR);
		return head;
	}

    void display()
    {
        if (topSmoke.running)
        {
			smokeTransformationContext.applyOn(drawEllipses);
        }
    }
}
