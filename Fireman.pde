import java.util.Map;
import java.util.function.Supplier;

public class Fireman implements Drawable
{
	Map<FiremanState, DisplayFun> actions = null;
	Map<FiremanState, StateFun> states = null;
	FiremanState state = FiremanState.STANDING;
	PVector pos = null;
	boolean mirrored = false;
	
	final PVector INITIAL_POSITION;
	PVector speed = new PVector();

	final TransformationContext transformationContext = new TransformationContext(translate).withParams(
		new Supplier() { public PVector get() { return pos; } }
	);
	final TransformationContext mirroredTransformationContext = prepareMirroredTransformationContext();
	
	Fireman(
		final Map<FiremanState, DisplayFun> actions,
		final Map<FiremanState, StateFun> states,
		FiremanState initState,
		PVector position
	)
	{
		this.actions = actions;
		this.states = states;
		state = initState;
		INITIAL_POSITION = position;
		pos = INITIAL_POSITION.copy();
	}
	
	private TransformationContext prepareMirroredTransformationContext()
	{
		final TransformationContext head = new TransformationContext(translate).withParams(
			new Supplier()
			{
				private final PVector offset = new PVector();
				public PVector get()
				{
					offset.x = pos.x + 500;
					offset.y = pos.y;
					return offset;
				}
			}
		);
		head.followedBy(scale).withParams(-1.f, 1.f);
		return head;
	}

	void changeState() throws IllegalStateException
	{
		final MyCallable c = states.getOrDefault(state, null);
		if (c == null)
		{
			throw new IllegalStateException(state + ": invalid fireman state");
		}
		c.call(this);
	}
	
	void display() throws IllegalStateException
	{
		if (!enterDoor.opening() && !enterDoor.opened())
		{
			return;
		}
		final MyCallable c = actions.getOrDefault(state, null);
		if (c == null)
		{
			throw new IllegalStateException(state + ": invalid fireman state");
		}
		
		if (state == FiremanState.WALKING_BACK)
		{
			mirroredTransformationContext.applyOn(c, this, new Object());
		}
		else
		{
			transformationContext.applyOn(c, this, new Object());
		}
	}

	boolean isOnInitialPosition()
	{
		return pos.equals(INITIAL_POSITION);
	}
	
	Fireman setSpeed(PVector newSpeed)
	{
		speed = newSpeed;
		return this;
	}
}

enum FiremanState
{
	WALKING,
	WALKING_SW,
	WALKING_NW,
	WALKING_BACK,
	STANDING,
	TURNING,
	
	HELPING,
	PULLING,
	FALLING,
	STANDINGUP,
	HURT,
	
	ENTERING,
	LOOKING,
	EQUIPPING,
	OOF,
	
	ENTERING_FIRE_TRUCK
}

static interface MyCallable
{
	void call(Fireman fireman, Object... args);
}

// Access Animation's const members only.
interface StateFun extends MyCallable {}

// Modify Animation's state.
interface DisplayFun extends MyCallable {}

abstract class WaitAfterAnimationFinishedStateFun implements StateFun
{
	float tTimeToWait = 1000;
	float tEndAction = 0.0f;
	
	WaitAfterAnimationFinishedStateFun() {}
	
	WaitAfterAnimationFinishedStateFun(float time)
	{
		tTimeToWait = time;
	}
	
	void startCountingIfHaventStarted()
	{
		// almost zero
		if (tEndAction < 0.1f && tEndAction > -0.1f)
		{
			tEndAction = millisWrapper();
		}
	}
}

abstract class StateRepeatFun implements StateFun
{
	boolean paused = false;
	float timer = 0.f;
	int repeats = 0;
	float holdOn = 0.f;
	
	StateRepeatFun(float holdOn)
	{
		this.holdOn = holdOn;
	}
	
	boolean isPaused() { return paused; }
}

interface WalkBackDisplayFun extends DisplayFun
{
	static final float SMALL_SIZE = 0.5;
	static final float THR_X = 370;
	
	boolean isSmall(Fireman fireman);
	float smallSize(Fireman fireman);
}

HashMap<FiremanState, DisplayFun> commonDisplays()
{
	HashMap<FiremanState, DisplayFun> displays = new HashMap();
	displays.put(FiremanState.WALKING_BACK, displayWalkingBack);
	displays.put(FiremanState.TURNING, displayTurning);
	displays.put(FiremanState.ENTERING_FIRE_TRUCK, displayEnteringFireTruck);
	return displays;
}

// Fireman 1
Map<FiremanState, StateFun> f1States()
{
	HashMap<FiremanState, StateFun> states = new HashMap();
	states.put(FiremanState.ENTERING, stateEntering);
	states.put(FiremanState.LOOKING, stateLooking);
	states.put(FiremanState.WALKING, stateWalking);
	states.put(FiremanState.WALKING_SW, stateWalkingSw);
	states.put(FiremanState.WALKING_NW, stateWalkingNw);
	states.put(FiremanState.STANDING, stateStanding);
	states.put(FiremanState.TURNING, stateTurning);
	states.put(FiremanState.HELPING, stateHelping);
	states.put(FiremanState.PULLING, statePulling);
	states.put(FiremanState.FALLING, stateFalling);
	states.put(FiremanState.STANDINGUP, stateStandingUp);
	states.put(FiremanState.HURT, stateHurt);
	states.put(FiremanState.WALKING_BACK, stateWalkingBack);
	states.put(FiremanState.ENTERING_FIRE_TRUCK, stateEnteringFireTruck);
	return states;
}

Map<FiremanState, DisplayFun> f1Displays()
{
	HashMap<FiremanState, DisplayFun> displays = commonDisplays();
	displays.put(FiremanState.WALKING, displayWalking);
	displays.put(FiremanState.WALKING_SW, displayWalking);
	displays.put(FiremanState.WALKING_NW, displayWalking);
	displays.put(FiremanState.STANDING, displayStanding);
	displays.put(FiremanState.HELPING, displayHelping);
	displays.put(FiremanState.PULLING, displayPulling);
	displays.put(FiremanState.FALLING, displayFalling);
	displays.put(FiremanState.STANDINGUP, displayStandingUp);
	displays.put(FiremanState.HURT, displayHurt);
	displays.put(FiremanState.ENTERING, displayEntering);
	displays.put(FiremanState.LOOKING, displayLooking);
	return displays;
}

// Fireman 2
Map<FiremanState, StateFun> f2States()
{
	HashMap<FiremanState, StateFun> states = new HashMap();
	states.put(FiremanState.STANDING, stateStanding2);
	states.put(FiremanState.WALKING_BACK, stateWalkingBack);
	states.put(FiremanState.WALKING_SW, stateWalkingSw2);
	states.put(FiremanState.WALKING_NW, stateWalkingNw);
	states.put(FiremanState.WALKING, stateWalking2);
	states.put(FiremanState.EQUIPPING, stateExtinguishing);
	states.put(FiremanState.OOF, stateOof);
	states.put(FiremanState.ENTERING_FIRE_TRUCK, stateEnteringFireTruck);
	return states;
}

Map<FiremanState, DisplayFun> f2Displays()
{
	HashMap<FiremanState, DisplayFun> displays = commonDisplays();
	displays.put(FiremanState.WALKING, displayWalkingWithExtinguisher);
	displays.put(FiremanState.WALKING_SW, displayWalkingWithExtinguisher);
	displays.put(FiremanState.WALKING_NW, displayWalkingWithExtinguisher);
	displays.put(FiremanState.STANDING, displayStanding);
	displays.put(FiremanState.EQUIPPING, displayEquipping);
	displays.put(FiremanState.OOF, displayOof);
	return displays;
}

// States
StateFun stateWalkingNw = new StateFun()
{
	final PVector SPEED_NORTH_WEST = new PVector(-16.5, -3.8).div(ANIMATION_FPS_DECIMATE_FACTOR);
	void call(Fireman fireman, Object... args)
	{
		fireman.pos.add(SPEED_NORTH_WEST);
		if (fireman.pos.y < 380)
		{
			fireman.state = FiremanState.WALKING_BACK;
			if (fireman.pos.y < 390 && !fireExit.opened())
			{
				fireExit.open();
			}
			if (drawableSequence.sequence.indexOf(fireman) > drawableSequence.sequence.indexOf(kitchen))
			{
				drawableSequence.insertBefore(kitchen, fireman);
			}
		}
	}
};

StateFun stateWalkingSw = new StateFun()
{
	final PVector FIREMAN_SPEED_SW = new PVector(-15, 10).div(ANIMATION_FPS_DECIMATE_FACTOR);
	void call(Fireman fireman, Object... args)
	{
		fireman.pos.add(FIREMAN_SPEED_SW);
		if (fireman.pos.y > 470)
		{
			fireman.state = FiremanState.WALKING;
		}
	}
};

StateFun stateWalkingSw2 = new StateFun()
{
	final PVector FIREMAN2_SPEED_SW = new PVector(-4, 10).div(ANIMATION_FPS_DECIMATE_FACTOR);
	void call(Fireman fireman, Object... args)
	{
		fireman.pos.add(FIREMAN2_SPEED_SW);
		if (fireman1.isOnInitialPosition() && inRange(fireman.pos.x, 1300, 1350))
		{
			fireman1.state = FiremanState.WALKING_SW;
		}
		if (fireman.pos.y > 450)
		{
			fireman.state = FiremanState.WALKING;
		}
		else if (!ovenFire.extinguished && fireman.pos.y > 410)
		{
			fireman.state = FiremanState.EQUIPPING;
		}
	}
};

StateFun stateWalking = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		fireman.pos.add(fireman.speed);
		if (fireman.pos.x - person.position.x < 0 && inRange(fireman.pos.x - person.position.x, -250, -240))
		{
			fireman.state = FiremanState.TURNING;
		}
		else if (fireman.pos.x < 520)
		{
			fireman.state = FiremanState.WALKING_NW;
		}
		else if (fireman.pos.x < 100)
		{
			fireman.state = FiremanState.STANDING;
		}
	}
};

StateFun stateWalking2 = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		fireman.pos.add(fireman.speed);
		if (fireman.pos.x < 380)
		{
			fireman.state = FiremanState.WALKING_NW;
		}
		else if (fireman.pos.x < 100)
		{
			fireman.state = FiremanState.STANDING;
		}
	}
};

StateFun stateWalkingBack = new StateFun()
{
	final PVector speedWalkingBack = new PVector(25, -2).div(ANIMATION_FPS_DECIMATE_FACTOR);
	boolean multiplied = false;
	void call(Fireman fireman, Object... args)
	{
		if (fireman == fireman2 && 
			drawableSequence.sequence.indexOf(fireman) > drawableSequence.sequence.indexOf(fireTruck) && 
			fireman.pos.x + 300 > fireTruck.position.x)
		{
			drawableSequence.insertBefore(fireTruck, fireman);
		}
		if (!multiplied && displayWalkingBack.isSmall(fireman))
		{
			speedWalkingBack.mult(WalkBackDisplayFun.SMALL_SIZE);
			multiplied = true;
		}
		fireman.pos.add(speedWalkingBack);
		if (inRange(fireman.pos.x, fireTruck.position.x + 150, fireTruck.position.x + 300) && fireTruck.speed <= 0.1f)
		{
			fireman.state = FiremanState.ENTERING_FIRE_TRUCK;
			if (fireman == fireman1)
			{
				fireTruck.frontDoor.open();
			}
		}
	}
};

StateFun stateTurning = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		if (!displayTurning.animation.ended())
		{
			return;
		}
		if (displayTurning.animation.isPlayingFwd())
		{
			fireman.state = FiremanState.HELPING;
		}
		else
		{
			person.play();
			fireman.state = FiremanState.WALKING;
		}
	}
};

StateFun stateHelping = new StateRepeatFun(900)
{
	final int MAX_REPEATS = 3;
	void call(Fireman fireman, Object... args)
	{
		if (repeats == MAX_REPEATS)
		{
			fireman.state = FiremanState.PULLING;
		}

		if (!displayHelping.animation.ended())
		{
			return;
		}
		
		if (displayHelping.animation.isPlayingFwd())
		{
			if (!paused)
			{
				timer = millisWrapper();
				paused = true;
			}
			if (timerIsReady(timer, holdOn))
			{
				displayHelping.animation.playBwd();
				paused = false;
				++repeats;
			}
		}
		else
		{
			displayHelping.animation.playFwd();
		}
	}
};

StateFun statePulling = new WaitAfterAnimationFinishedStateFun(2500)
{
	void call(Fireman fireman, Object... args)
	{
		startCountingIfHaventStarted();
		if (timerIsReady(tEndAction, tTimeToWait))
		{
			fireman.state = FiremanState.FALLING;
			person.play();
		}
	}
};

StateFun stateFalling = new WaitAfterAnimationFinishedStateFun(2000)
{
	void call(Fireman fireman, Object... args)
	{
		if (!displayFalling.animation.ended())
		{
			return;
		}
		startCountingIfHaventStarted();
		if (timerIsReady(tEndAction, tTimeToWait))
		{
			fireman.state = FiremanState.STANDINGUP;
		}
	}
};

StateFun stateStandingUp = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		if (displayStandingUp.animation.ended())
		{
			fireman.state = FiremanState.HURT;
		}
	}
};

StateFun stateHurt = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		if (displayHurt.animation.ended())
		{
			fireman.state = FiremanState.WALKING;
		}
	}
};

StateFun stateExtinguishing = new StateRepeatFun(2500)
{
	void call(Fireman fireman, Object... args)
	{
		if (!displayEquipping.extinguishingAnimation.ended())
		{
			return;
		}
		switch (repeats)
		{
			// the following two cases are the same
			case 0:
			case 2:
				if (!paused)
				{
					timer = millisWrapper();
				}
				paused = true;
				foam.play();
				if (timerIsReady(timer, holdOn))
				{
					foam.pause();
					++repeats;
					paused = false;
					if (repeats == 2)
					{
						displayEquipping.extinguishingAnimation.playBwd();
					}
				}
				break;
			
			case 1:
				if (foam.isEmpty())
				{
					foam.play();
					++repeats;
				}
				break;
			
			case 3:
				ovenFire.extinguished = true;
				fireman.state = FiremanState.OOF;

			default:
				break;
		}
	}
};


StateFun stateOof = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		if (displayOof.animation.ended())
		{
			fireman.state = FiremanState.WALKING_SW;
		}
	}
};

StateFun stateStanding = new StateFun()
{
	void call(Fireman fireman, Object... args) {}
};

StateFun stateStanding2 = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		if (inRange(fireman1.pos.x, 1300, 1350))
		{
			fireman.state = FiremanState.WALKING_SW;
		}
	}
};

StateFun stateEntering = new StateFun()
{
	void call(Fireman fireman, Object... args)
	{
		if (!displayEntering.loaded)
		{
			return;
		}
		if (displayEntering.animation.frame == 6)
		{
			turnedOffLight = false;
		}
		if (displayEntering.animation.ended())
		{
			fireman.state = FiremanState.LOOKING;
		}
	}
};

StateFun stateLooking = new StateFun()
{
	float tLookStart = 0.f;
	final static float TIME_TO_LOOK = 700.f;
	
	void call(Fireman fireman, Object... args)
	{
		if (tLookStart < 0.01f)
		{
			tLookStart = millisWrapper();
		}
		if (timerIsReady(tLookStart, 2 * TIME_TO_LOOK))
		{
			fireman.state = FiremanState.WALKING_SW;
		}
		else if (timerIsReady(tLookStart, TIME_TO_LOOK))
		{
			displayLooking.animation.incFrame();
		}
	}
};

StateFun stateEnteringFireTruck = new StateFun()
{
	void call(Fireman fireman, Object... args) {}
};

///////////////////////////////
///////////////////////////////
// Display functions
///////////////////////////////
///////////////////////////////

class DisplayWalkingFun implements DisplayFun
{
	private Animation walking = null;

	void call(Fireman fireman, Object... args)
	{
		if (walking == null)
		{
			walking = new Animation("anim/fireman/walking/")
				.fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.setRepeat(true)
				.attach(new Animation("anim/fireman/walking/light/")
					.attach(prepareFireTruckTransformationContext())
				).play();
		}
		walking.display();
	}
};
DisplayWalkingFun displayWalking = new DisplayWalkingFun();

class DisplayWalkingWithExtinguisherFun implements DisplayFun
{
	Animation walking = null;
	void call(Fireman fireman, Object... args)
	{
		if (walking == null)
		{
			walking = new Animation("anim/fireman/walking with extinguisher")
				.attach(
					new Animation("anim/fireman/walking with extinguisher/light/light.png", 400, 0, true)
						.attach(prepareFireTruckTransformationContext())
				)
				.fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.setRepeat(true)
				.play();
		}
		walking.display();
	}
};
DisplayWalkingWithExtinguisherFun displayWalkingWithExtinguisher = new DisplayWalkingWithExtinguisherFun();

WalkBackDisplayFun displayWalkingBack = new WalkBackDisplayFun()
{
	class SmallTransformationContext
	{
		public TransformationContext smallTransformationContext = null;
		public float smallFactor = 0.f;
		public Animation walking = null;

		public SmallTransformationContext()
		{
			smallTransformationContext = prepareDisplaySmallTransformationContext();
		}

		private TransformationContext prepareDisplaySmallTransformationContext()
		{
			final TransformationContext head = new TransformationContext(translate).withParams(
				new Supplier()
				{
					private final PVector vec = new PVector();
					public PVector get()
					{
						vec.x = (0.87f - smallFactor) * walking.getCurrentFrame().width;
						vec.y = (0.87f - smallFactor) * walking.getCurrentFrame().height;
						return vec;
					}
				}
			);
			head.followedBy(scale).withParams(new Supplier() { public Float get() { return smallFactor; } });
			return head;
		}
	}
	private SmallTransformationContext smallTransformationContext = null;

	void call(Fireman fireman, Object... args)
	{
		if (smallTransformationContext == null)
		{
			smallTransformationContext = new SmallTransformationContext();
		}

		final Animation walking = (fireman == fireman1) ? displayWalking.walking : displayWalkingWithExtinguisher.walking;

		if (isSmall(fireman))
		{
			smallTransformationContext.smallFactor = smallSize(fireman);
			smallTransformationContext.walking = walking;
			smallTransformationContext.smallTransformationContext.applyOn(walking);
		}
		else
		{
			walking.display();
		}
	}
	
	boolean isSmall(Fireman fireman)
	{
		return fireman.pos.x > THR_X;
	}
	
	float smallSize(Fireman fireman)
	{
		return map(fireman.pos.x, THR_X, 1920, 0.4f, 0.2f);
	}
};

DisplayFun displayStanding = new DisplayFun()
{
	private Drawable standing = null;
	private TransformationContext transformationContext = null;

	private float standingWidth = 0.f;
	private float standingHeight = 0.f;
	private float smallFactor = 0.f;

	private Drawable prepareData()
	{
		final PImage temp = loadImage("imgs/fireman.png");
		standingWidth = temp.width;
		standingHeight = temp.height;

		return new Drawable()
		{
			private PImage firemanStanding = temp;
			private PImage light           = loadImage("imgs/light/fireman.png");

			private final TransformationContext fireTruckLightTransformationContext = prepareFireTruckTransformationContext();

			public void display()
			{
				g_canvas.image(firemanStanding, 0, 0);
				fireTruckLightTransformationContext.applyOn(light);
			}
		};
	}

	private TransformationContext prepareTransformationContext()
	{
		final TransformationContext head = new TransformationContext(translate).withParams(
			new Supplier()
			{
				private final PVector vec = new PVector();
				public PVector get()
				{
					vec.x = (1 - smallFactor) * standingWidth;
					vec.y = (1 - smallFactor) * standingHeight;
					return vec;
				}
			}
		);
		head.followedBy(scale).withParams(smallFactor);
		return head;
	}

	void call(Fireman fireman, Object... args)
	{
		if (standing == null)
		{
			standing = prepareData();
			transformationContext = prepareTransformationContext();
		}
		
		final boolean displaySmall = displayWalkingBack.isSmall(fireman) && ovenFire.extinguished;
		if (displaySmall)
		{
			smallFactor = displayWalkingBack.smallSize(fireman);
			transformationContext.applyOn(standing);
		}
		else
		{
			standing.display();
		}
	}
};

class DisplayEquippingFun implements DisplayFun
{
	Animation extinguishingAnimation = null;
	
	void call(Fireman fireman, Object... args)
	{
		if (extinguishingAnimation == null)
		{
			extinguishingAnimation = new Animation("anim/fireman/extinguishing")
				.attach(new Animation("anim/fireman/extinguishing/light/light.png", 400, 0, true)
					.attach(prepareFireTruckTransformationContext())
				).fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.play();
			extinguishingAnimation.play();
		}
		extinguishingAnimation.display();
	}
};
DisplayEquippingFun displayEquipping = new DisplayEquippingFun();

class DisplayOofFun implements DisplayFun
{
	Animation animation = null;
	void call(Fireman fireman, Object... args)
	{
		// load the animation in draw call, after Processing is loaded
		if (animation == null)
		{
			animation = new Animation("anim/fireman/oof/oof.png", 400, 0)
				.attach(new Animation("anim/fireman/oof/light.png", 400, 0)
					.attach(prepareFireTruckTransformationContext())
				).fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.play();
		}
		
		animation.display();
	}
}
DisplayOofFun displayOof = new DisplayOofFun();

class DisplayTurningFun implements DisplayFun
{
	Animation animation = null;
	void call(Fireman fireman, Object... args)
	{
		if (animation == null)
		{
			animation = new Animation("anim/fireman/face the person/face-to-person_0", 5)
				.attach(new Animation("anim/fireman/face the person/light/")
					.attach(prepareFireTruckTransformationContext())
				).fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.play();
		}
		
		animation.display();
	}
};
DisplayTurningFun displayTurning = new DisplayTurningFun();

class DisplayHelpingFun implements DisplayFun
{
	Animation animation = null;

	private TransformationContext shakeTransformationContext = null;
	private TransformationContext prepareShakeTransformationContext()
	{
		return new TransformationContext(translate).withParams(
			new Supplier()
			{
				private final PVector returnValue = new PVector(0, 0);
				public PVector get()
				{
					returnValue.x = ((StateRepeatFun)stateHelping).isPaused() ? random(-2.f, 2.f) : 0.f;
					return returnValue;
				}
			}
		);
	}

	void call(Fireman fireman, Object... args)
	{
		if (animation == null)
		{
			animation = new Animation("anim/fireman/help/")
				.attach(new Animation("anim/fireman/help/light/light.png", 400, 0)
					.attach(prepareFireTruckTransformationContext())
				).fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.play();
			shakeTransformationContext = prepareShakeTransformationContext();
		}
		
		if (((StateRepeatFun)stateHelping).repeats > 0)
		{
			shakeTransformationContext.applyOn(animation);
		}
		else
		{
			animation.display();
		}
	}
}
DisplayHelpingFun displayHelping = new DisplayHelpingFun();

DisplayFun displayPulling = new DisplayFun()
{
	Drawable pulling = null;
	TransformationContext transformationContext = null;

	private Drawable prepareData()
	{
		return new Drawable()
		{
			final PImage pullingImage = loadImage("imgs/pull-out-struggle_01.png");
			final PImage pullingLightImage = loadImage("imgs/pull-out-struggle_01 light.png");
			final Animation struggleFace = new Animation("anim/fireman/struggle face")
				.fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.play();

			final TransformationContext fireTruckLightTransformationContext = prepareFireTruckTransformationContext();

			public void display()
			{
				g_canvas.image(pullingImage, 0, 0);
				fireTruckLightTransformationContext.applyOn(pullingLightImage);
				struggleFace.display();
			}
		};
	}

	private TransformationContext prepareTransformationContext()
	{
		return new TransformationContext(translate)
			.withParams(new Supplier()
			{
				private final PVector vec = new PVector(0, 0);
				public PVector get() 
				{
					vec.x = random(-2.f, 2.f);
					return vec;
				}
			});
	}

	void call(Fireman fireman, Object... args)
	{
		if (pulling == null)
		{
			pulling = prepareData();
			transformationContext = prepareTransformationContext();
		}
		
		transformationContext.applyOn(pulling);
	}
};

class DisplayFallingFun implements DisplayFun
{
	private Animation animation = null;
	private TransformationContext transformationContext = null;

	void call(Fireman fireman, Object... args)
	{
		if (animation == null)
		{
			transformationContext = new TransformationContext(translate).withParams(-200.f, 0.f);
			animation = new Animation("anim/fireman/fall/fall.png", 600, 400)
				.attach(new Animation("data/anim/fireman/fall/light/light.png", 600, 400)
					.attach(prepareFireTruckTransformationContext())
				).fpsDecimationRatio(2)
				.play();
		}
		transformationContext.applyOn(animation);
	}
};
DisplayFallingFun displayFalling = new DisplayFallingFun();

class DisplayStandingUpFun implements DisplayFun
{
	Animation animation = null;
	private TransformationContext transformationContext = null;

	void call(Fireman fireman, Object... args)
	{
		if (animation == null)
		{
			transformationContext = new TransformationContext(translate).withParams(-200.f, 0.f);
			animation = new Animation("data/anim/fireman/stand up/stand up.png", 600, 0)
				.attach(new Animation("anim/fireman/stand up/light/light.png", 600, 0)
					.attach(prepareFireTruckTransformationContext())
				).fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
			.play();
		}
		transformationContext.applyOn(animation);
	}
}
DisplayStandingUpFun displayStandingUp = new DisplayStandingUpFun();

class DisplayHurtFun implements DisplayFun
{
	Animation animation = null;
	void call(Fireman fireman, Object... args)
	{
		if (animation == null)
		{
			animation = new Animation("anim/fireman/hurt")
				.attach(new Animation("anim/fireman/hurt/light/light.png", 400, 0)
					.attach(prepareFireTruckTransformationContext())
				).fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
			.play();
		}
		animation.display();
	}
}
DisplayHurtFun displayHurt = new DisplayHurtFun();

class DisplayEnteringFun implements DisplayFun
{
	Animation animation = null;
	boolean loaded = false;
	
	void call(Fireman fireman, Object... args)
	{
		if (animation == null)
		{
			animation = new Animation("anim/fireman/enter")
				.fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.attach(new Animation("anim/fireman/enter/light")
					.attach(prepareFireTruckTransformationContext())
				)
				.play();
			loaded = true;
		}
		animation.display();
	}
};
DisplayEnteringFun displayEntering = new DisplayEnteringFun();

class DisplayLookingFun implements DisplayFun
{
	Animation animation = null;
	
	void call(Fireman fireman, Object... args)
	{
		if (animation == null)
		{
			animation = new Animation("anim/fireman/look/")
				.fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR)
				.attach(new Animation("anim/fireman/look/light/")
					.attach(prepareFireTruckTransformationContext())
				).play();
		}
		animation.display();
	}
};
DisplayLookingFun displayLooking = new DisplayLookingFun();

DisplayFun displayEnteringFireTruck = new DisplayFun()
{
	void call(Fireman fireman, Object... args) {}
};

//////////////////////
// auxiliary functions
//////////////////////

boolean inRange(float value, float lo, float hi)
{
	return value >= lo && value <= hi;
}

boolean timerIsReady(float timerToEval, float delta)
{
	return timerToEval > 0.0f && millisWrapper() > (timerToEval + delta);
}
