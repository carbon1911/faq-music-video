///////////////////////////////
///////////////////////////////
// Knock
///////////////////////////////
///////////////////////////////

class Knock implements Drawable
{
	PImage im = null;
	int visibility = 255;
	int disappearingRate = 0;
	boolean active = false;
	TransformationContext tc = null;
	
	Knock(PImage image, int disappearingRate, float rotation, PVector position)
	{
		im = image;
		this.disappearingRate = disappearingRate;
		tc = prepareTransformationContext(position, rotation);
	}
	
	void display()
	{
		if (!active)
		{
			return;
		}

		tc.applyOn(im);

		if (!isDead())
		{
			visibility -= disappearingRate;
		}
	}
	
	boolean isDead()
	{
		return visibility <= 0;
	}
	
	boolean isActive()
	{
		return active && isDead();
	}
	
	void activate()
	{
		if (!active)
		{
			knockShakeCanvasTransformationContext.tShakeStart = millisWrapper();
		}
		active = true;
	}
	
	void deactivate()
	{
		active = false;
	}

	private TransformationContext prepareTransformationContext(final PVector position, float rotation)
	{
		TransformationContext head = new TransformationContext(translate).withParams(position);
		head.followedBy(rotate).withParams(rotation)
		.followedBy(transparency).withParams(
			new Supplier()
			{
				public Integer get() { return visibility; }
			}
		);
		return head;
	}
};

///////////////////////////////
///////////////////////////////
// KnockManager
///////////////////////////////
///////////////////////////////

class KnockManager implements Drawable
{
	final ArrayList<Knock> knocks = new ArrayList<Knock>();
	int currentActiveKnockIndex = 0;

	final static float TIME_BETWEEN_KNOCKS = 2500;

	void add(final Knock knock)
	{
		knocks.add(knock);
	}

	Knock at(int index)
	{
		return knocks.get(index);
	}

	@Override
	void display()
	{
		for (final Knock k : knocks)
		{
			k.display();
		}
	}

	void activateNextKnock()
	{
		if (currentActiveKnockIndex >= knocks.size())
		{
			return;
		}
		at(currentActiveKnockIndex).activate();
		++currentActiveKnockIndex;
	}

	float getTimeBetweenKnocks()
	{
		return TIME_BETWEEN_KNOCKS;
	}

	float getTimeBetweenKnocks(int index)
	{
		return index * TIME_BETWEEN_KNOCKS;
	}
}

///////////////////////////////
///////////////////////////////
// KnockShakeTransformationContext
///////////////////////////////
///////////////////////////////

class KnockShakeTransformationContext
{
	TransformationContext transformationContext = null;
	float tShakeDiff = 0.0f;
	float tShakeStart = 0.f;

	final static int INTENSITY = 11;
	final static float DURATION = 350;

	KnockShakeTransformationContext()
	{
		transformationContext = prepareKnockShakeCanvasTransformationContext();
	}

	boolean isApplicable()
	{
		return tShakeStart > 0.0f && tShakeDiff < DURATION;
	}

	void setDiff()
	{
		tShakeDiff = millisWrapper() - tShakeStart;
	}

	private TransformationContext prepareKnockShakeCanvasTransformationContext()
	{
		final TransformationContext head = new TransformationContext(translate, getGraphics()).withParams(
			new Supplier() 
			{
				private final PVector shakeIntensity = new PVector();
				public PVector get()
				{
					shakeIntensity.set(
						0, map(tShakeDiff, DURATION, 0, 0, 1 * random(-INTENSITY, INTENSITY))
					);
					return shakeIntensity;
				}
			}
		);
		return head;
	}
}
