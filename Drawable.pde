static final float BNW_TINT = 30;

interface Drawable
{
	void display();
}

interface TurnedOffLight
{
	void setLightState(boolean lightTurnedOff);
}

interface ConditionallyDisplayable
{
	boolean isDisplayable();
}

interface AcceptsConditionalLight
{
	Drawable setConditionalLight(ConditionalLight cdl);
}

abstract class InitialText implements Drawable, ConditionallyDisplayable
{
	PImage text = null;
	InitialText(PImage textImage)
	{
		text = textImage;
		text.resize(width, 0);
	}
	
	void display()
	{
		if (!isDisplayable())
		{
			return;
		}
		image(text, 0, 0);
	}
}

class FullscreenDisplayable implements Drawable, TurnedOffLight, AcceptsConditionalLight
{
	PImage toDisplay = null;
	boolean bnw = false;
	PGraphics bnwCanvas = null;
	ConditionalLight cdl = null;
	
	FullscreenDisplayable(final PImage image)
	{
		toDisplay = image;
		
		bnwCanvas = createGraphics(toDisplay.width, toDisplay.height, P2D);

		getBnwTransformationContext(bnwCanvas).applyOn(
			new Runnable()
			{
				public void run() { new TransformationContext(tint, bnwCanvas).withParams(BNW_TINT).applyOn(toDisplay); }
			}
		);
	}
	
	void display()
	{
		g_canvas.image(bnw ? bnwCanvas : toDisplay, 0, 0);
		if (cdl != null)
		{
			cdl.display();
		}
	}
	
	void setLightState(boolean lightTurnedOff)
	{
		this.bnw = lightTurnedOff;
	}
	
	FullscreenDisplayable setConditionalLight(ConditionalLight cdl)
	{
		this.cdl = cdl;
		return this;
	}

	TransformationContext getBnwTransformationContext(PGraphics bnwCanvas)
	{
		final TransformationContext head = new TransformationContext(beginEndDraw, bnwCanvas);
		head.followedBy(new Transformation()
		{
			public void pre(PGraphics canvas) {}
			public void post(PGraphics canvas) { canvas.filter(GRAY); }
		});
		return head;
	}
}

class FullscreenDisplayableNoBNW implements Drawable
{
	PImage toDisplay = null;
	FullscreenDisplayableNoBNW(PImage image)
	{
		toDisplay = image;
	}
	
	void display()
	{
		g_canvas.image(toDisplay, 0, 0);
	}
}

class Car implements Drawable
{
	final PImage im = loadImage("imgs/car.png");
	final PImage headLights = loadImage("imgs/light/headlights.png");
	final PVector START_POSITION = new PVector(557, 595);
	final PVector DIRECTION = new PVector(-8, -4);
	PVector currentPosition = START_POSITION.copy();
	boolean driving = false;
	
	TransformationContext lightBufferTransformationContext = prepareLightBufferTransformationContext();
    TransformationContext movingTransformationContext = new TransformationContext(translate)
		.withParams(new Supplier() { PVector get() { return currentPosition; } });

	Runnable drawCar = new Runnable() { void run() { drawCar(); } };

	private TransformationContext prepareLightBufferTransformationContext()
	{
		final TransformationContext head = new TransformationContext(translate).withParams(new PVector(-92, -38));
		head.followedBy(blendMode).withParams(ADD);
		return head;
	}

	private void drawCar()
	{
		g_canvas.image(im, 0, 0);
		lightBufferTransformationContext.applyOn(headLights);
	}

	void display()
	{
		movingTransformationContext.applyOn(drawCar);
	}

	void changeState()
	{
		if (driving || (g_song != null && g_song.isPlaying() && g_song.position() > 83))
		{
			currentPosition.add(DIRECTION);
		}
	}
}

class Person implements Drawable
{
	Animation personAnimation = new Animation("anim/person/person ", 8)
            .fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR);
            
	Animation lightAnimation = new Animation("anim/person/light/")
            .fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR);
            
	boolean playing = false;
	PVector position = new PVector(770, 705);
	
	TransformationContext movingTransformationContext = new TransformationContext(translate)
		.withParams(new Supplier() { PVector get() { return position; } });
	TransformationContext fireTruckLightTransformationContext = prepareFireTruckTransformationContext();

	Runnable drawPerson = new Runnable() { void run() { drawPerson(); } };
	
	private void drawPerson()
	{
		personAnimation.display();
		if (roomLight.isDisplayable())
		{
			fireTruckLightTransformationContext.applyOn(lightAnimation);
		}
	}

	void display()
	{
		movingTransformationContext.applyOn(drawPerson);
	}
	
	void play()
	{
		personAnimation.play();
		lightAnimation.play();
	}
}

// TODO: get rid of the interfaces. Door is just a fancy wrapper
// for Animation. Try to solve the light stuff with Transformation
// and composition of Transformations.
class Door implements Drawable, TurnedOffLight, AcceptsConditionalLight
{
	Animation anim = null;
	boolean bnw = false;
	ConditionalLight cdl = null;
	
	TransformationContext bnwTransformationContext = new TransformationContext(tint).withParams(BNW_TINT);

	Door(Animation door)
	{
		anim = door;
	}
	
	void display()
	{
		if (bnw)
		{
			bnwTransformationContext.applyOn(anim);
		}
		else
		{
			anim.display();
		}

		if (cdl != null)
		{
			cdl.display();
		}
	}
	
	void setLightState(boolean lightTurnedOff)
	{
		this.bnw = lightTurnedOff;
	}
	
	Door setConditionalLight(ConditionalLight cdl)
	{
		this.cdl = cdl;
		return this;
	}
	
	void open()
	{
		anim.play();
		if (!anim.isPlayingFwd())
		{
			anim.playFwd();
		}
	}
	
	void close()
	{
		anim.play();
		if (anim.isPlayingFwd())
		{
			anim.playBwd();
		}
	}
	
	boolean opening()
	{
		return anim.isPlaying() && anim.isPlayingFwd() && !anim.ended();
	}
	
	boolean opened()
	{
		return anim.isPlayingFwd() && anim.ended();
	}
	
	boolean almostOpened()
	{
		return anim.frame == 1;
	}
}

class Fire implements Drawable, ConditionallyDisplayable
{
	PVector pos = null;
	Animation animation = null;
	boolean extinguished = false;

	final TransformationContext firePositionTransformationContext = prepareTransformationContext();
	TransformationContext prepareTransformationContext()
	{
		TransformationContext head = new TransformationContext(translate).withParams(
			new Supplier()
			{
				private PVector suppliedPos = new PVector();

				public PVector get()
				{
					suppliedPos = pos.copy();

					final float currImageHeight = animation.getCurrentFrame().height;
					final float currScaleClamped = map(currScale, INITIAL_SCALE, 1, 1e-5, 1);

					suppliedPos.y += (currImageHeight - currScaleClamped * currImageHeight);
					return suppliedPos;
				} 
			}
		);
		head.followedBy(scale).withParams(new Supplier() { public Float get() { return 1 / currScale; } })
			 .followedBy(blendMode).withParams(ADD);
		return head;
	}

	Fire(Animation animation)
	{
		this.animation = animation;
	}
	
	boolean isDisplayable()
	{
		return vert.running;
	}
	
	void display()
	{
		if (!isDisplayable())
		{
			return;
		}
		firePositionTransformationContext.applyOn(animation);
	}
	
	Fire setPosition(PVector newPosition)
	{
		pos = newPosition;
		return this;
	}
}

///////////////////////////////
///////////////////////////////
// DrawableSequence
///////////////////////////////
///////////////////////////////

class DrawableSequence implements Drawable
{
	final LinkedList<Drawable> sequence = new LinkedList();
	TransformationContext transformationContext = null;

	DrawableSequence()
	{
		sequence.add(skies);
		sequence.add(car);
		sequence.add(background);
		sequence.add(outsideLight);
		sequence.add(semaphoreLight);
		sequence.add(bridgeLight);
		sequence.add(housesLight);
		sequence.add(fireTruck);
		sequence.add(streetLampsLight);
		sequence.add(kitchen);
		sequence.add(fireExit);
		sequence.add(fireExitLight);
		sequence.add(roomLight);
		sequence.add(ceilingLight);
		//l.add(openDoorLight);
		sequence.add(person);
		sequence.add(earphone);
		sequence.add(vert);
		sequence.add(ovenFire);
		sequence.add(oven);
		//l.add(staticSmoke);
		sequence.add(topSmoke);
		sequence.add(fireman2);
		sequence.add(fireman1);
		sequence.add(foam);
		sequence.add(enterDoor);
		sequence.add(knockManager);
		// sequence.add(unstrungHarp);
		// sequence.add(initialFAQ);

		transformationContext = prepareTransformationContext();
	}

	TransformationContext prepareTransformationContext()
	{
		final TransformationContext head = new TransformationContext(beginEndDraw, g_canvas);
		return head;
	}

	void insertBefore(final Drawable drawAfter, final Drawable drawBefore)
	{
		sequence.remove(drawBefore);
		sequence.add(sequence.indexOf(drawAfter), drawBefore);
	}

	@Override
	void display()
	{
		for (final Drawable d : sequence)
		{
			if (d instanceof TurnedOffLight)
			{
				final TurnedOffLight dLight = (TurnedOffLight)d;
				dLight.setLightState(turnedOffLight);
			}
			d.display();
		}
	}
}
