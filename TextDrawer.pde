class TextDrawer
{
    float currentHeight = 0;
    
    void nextLine()
    {
        currentHeight += textAscent();
    }
    
    void resetCursor()
    {
        currentHeight = 0;
    }
    
    void printText(final String textToPrint)
    {
        nextLine();
        text(textToPrint, 32, currentHeight);
        if (currentHeight > SCREEN_HEIGHT)
        {
            resetCursor();
        }
    }
}

class DebugTextDrawer
{
    public TextDrawer textDrawer = null;
    private TransformationContext styleTransformationContext = null;
    private final Runnable printDebugInfo = new Runnable()
    {
        public void run()
        {
            textDrawer.printText(String.format("scale: %f", currScale));
            if (playMode == PlayMode.WITH_SONG)
            {
                textDrawer.printText(String.format("sec: %f", g_song.position()));
            }
            textDrawer.printText("FPS: " + frameRate);
        }
    };

    public DebugTextDrawer()
    {
        textDrawer = new TextDrawer();
        styleTransformationContext = prepareTransformationContext();
    }

    public void printDebugInfo()
    {
        styleTransformationContext.applyOn(printDebugInfo);
    }

    private TransformationContext prepareTransformationContext()
    {
        final TransformationContext head = new TransformationContext(fill, getGraphics()).withParams(  
            new Supplier() { public Integer get() { return turnedOffLight ? 255 : 0; } }
        );
        head.followedBy(stroke).withParams(0.f, 0.f, 0.f);
        return head;
    }
}
