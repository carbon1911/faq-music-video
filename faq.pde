import processing.sound.*;
import java.util.*;

enum PlayMode
{
	DEBUG,
	WITH_SONG,
	EXPORT
}

// sounds
PlayMode playMode = null;
static SoundFile g_song = null;
float songAmplitude = 0.5f;
float amplitudeDiff = 0.1f;

// scene-related global variables/constants
boolean debugInfo = false;
static boolean loaded = false;
static boolean playing = true;
static boolean turnedOffLight = true;
DebugTextDrawer textDrawer = null;
PGraphics g_canvas = null;
DrawableSequence drawableSequence = null;

static final float COMPENSATION = 0.0002;

//TODO: this variable -- to config
static final float AVG_FPS = 24;

// 0.00461 = 88 secs / 24 fps if init scale = 1.4055 and final scale = 1.0
// 0.00317 = 88 secs / 24 fps if init scale = 1 and final scale = 0.715
static final float SCALE_PER_SEC = 0.00461 + COMPENSATION;

//TODO: move to config
final int SCREEN_WIDTH = 1600;
final int SCREEN_HEIGHT = 900;
final float INITIAL_SCALE = 1.4055f; 
float currScale = INITIAL_SCALE;

// animation-related variables
static final int ANIMATION_FPS_DECIMATE_FACTOR = 3;

final static float TIME_TO_DOOR = 3000;
static float tVideoStart = 0.0f;

TransformationContext mainCanvasTransformationContext = null;
final Runnable mainCanvasDraw = new Runnable()
{
	public void run()
	{
		mainCanvasTransformationContext.applyOn(g_canvas);
	}
};

// images
static PImage imageScene = null;

// scene objects
Person person = null;
FireTruck fireTruck = null;
Earphone earphone = null;
Fire ovenFire = null;

Fireman fireman1 = null;
Fireman fireman2 = null;

// TODO: this class is bad abstraction
static FullscreenDisplayable kitchen = null;
static FullscreenDisplayable oven = null;

// TODO: this class is bad abstraction
FullscreenDisplayableNoBNW endingFAQ = null;
FullscreenDisplayableNoBNW background = null;
FullscreenDisplayableNoBNW skies = null;

Car car = null;

static Door enterDoor = null;
static Door fireExit = null;

static InitialText unstrungHarp = null;
static InitialText initialFAQ = null;

// light
static RoomLight roomLight = null;
static RoomLight openDoorLight = null;
static CeilingLight ceilingLight = null;
Drawable fireExitLight = null;
Drawable streetLampsLight = null;
Drawable semaphoreLight = null;
Drawable bridgeLight = null;
Drawable housesLight = null;
Drawable outsideLight = null;

// particles
StaticSmoke staticSmoke = null;
ParticleSystem vert = null;
ParticleSystem topSmoke = null;
ParticleSystem foam = null;

final KnockManager knockManager = new KnockManager();
KnockShakeTransformationContext knockShakeCanvasTransformationContext = null;

//TODO: try to remove all the following variables

static PVector vectToCorner = new PVector();

// positions
final static PVector FIRE_EXIT_POS = new PVector(216, 380);

void settings()
{
	//fullScreen(P2D);

	settings = new SettingsParser().parse("config.txt");

	size(Integer.parseInt(settings.get("SCREEN_WIDTH")), Integer.parseInt(settings.get("SCREEN_HEIGHT")), P2D);
}

void setup()
{
	final Map<String, String> settings = new SettingsParser().parse("config.txt");
	playMode = PlayMode.valueOf(settings.get("playMode"));
	debugInfo = Boolean.parseBoolean(settings.get("debugInfo"));

	frameRate(AVG_FPS);
	// text
	textSize(32);
	fill(0, 0, 0);
	text("Loading...", width / 2, height / 2);
	
	g_song = playMode == PlayMode.WITH_SONG ? new SoundFile(this, "data/faq.mp3", true) : null;
}

void load()
{
	// This must stay the first statement as some Transformations already make use of the main canvas.
	g_canvas = createGraphics(1920, 1080, P2D);

	mainCanvasTransformationContext = prepareMainCanvasTransformationContext();
	knockShakeCanvasTransformationContext = new KnockShakeTransformationContext();

	textDrawer = new DebugTextDrawer();

	// load images
	imageScene = loadImage("imgs/sketch.png");

	final PImage knockImage = loadImage("imgs/knock.png");
	knockManager.add(new Knock(knockImage, 10, random(-PI/6, PI/6), new PVector(1537, 493)));
	knockManager.add(new Knock(knockImage, 18, random(-PI/6, PI/6), new PVector(1603, 684)));
	
	// setup the variables
	fireman1 = new Fireman(f1Displays(), f1States(), 
		FiremanState.ENTERING,
		new PVector(1530, 350))
		.setSpeed(new PVector(-25, 0).div(ANIMATION_FPS_DECIMATE_FACTOR));
	fireman2 = new Fireman(f2Displays(), f2States(), 
		FiremanState.STANDING,
		new PVector(1515, 340))
		.setSpeed(new PVector(-20, 0).div(ANIMATION_FPS_DECIMATE_FACTOR));
	
	person = new Person();
	fireTruck = new FireTruck();
	earphone = new Earphone();
	
	kitchen = new FullscreenDisplayable(imageScene);
	oven = new FullscreenDisplayable(loadImage("imgs/oven.png"))
		.setConditionalLight(new OpenDoorLight(loadImage("imgs/light/oven.png"), new PVector(1508, 762)));
	endingFAQ = new FullscreenDisplayableNoBNW(loadImage("imgs/text/FAQ.png"));
	
	background = new FullscreenDisplayableNoBNW(loadImage("imgs/bckg.png"));
	skies = new FullscreenDisplayableNoBNW(loadImage("imgs/skies.png"));
	
	car = new Car();
	
	unstrungHarp = new InitialText(loadImage("imgs/text/UH.png"))
	{
		public boolean isDisplayable()
		{
			return playing && inRange(millisSinceFirstDraw(), 3000, 8000);
		}
	};
	
	initialFAQ = new InitialText(loadImage("imgs/text/FAQ small.png"))
	{
		public boolean isDisplayable()
		{
			return playing && inRange(millisSinceFirstDraw(), 10000, 16000);
		}
	};
	
	enterDoor = new Door(new Animation("anim/enter door/").position(new PVector(1600, 313)))
		.setConditionalLight(new OpenDoorLight(loadImage("imgs/light/enter door.png"), new PVector(1837, 316)));
	fireExit = new Door(new Animation("anim/fire exit/").position(FIRE_EXIT_POS))
		.setConditionalLight(new RoomLight(loadImage("imgs/light/exit door 5.png"), FIRE_EXIT_POS)
		{
			public boolean isDisplayable()
			{
				return fireExit.opened();
			}
		}
	);

	roomLight = new RoomLight(loadImage("imgs/light.png"), new PVector(492, 164))
	{
		public boolean isDisplayable()
		{
			return fireTruck.displaying;
		}
	};
	
	openDoorLight = new OpenDoorLight(loadImage("imgs/light open door.png"), new PVector(762, 0));
	ceilingLight = new CeilingLight(loadImage("imgs/ceiling light persp.png"));
	
	fireExitLight = new Drawable()
	{
		private final PImage im = loadImage("imgs/light/fire exit.png");
		private final TransformationContext transformationContext = prepareLightTransformationContext(new PVector(78, 54));

		public void display()
		{
			transformationContext.applyOn(im);
		}
	};

	streetLampsLight = new Drawable()
	{
		private PImage im = loadImage("imgs/light/street lamps.png");
		private TransformationContext transformationContext = prepareTransformationContext();

		private TransformationContext prepareTransformationContext()
		{
			final TransformationContext head = new TransformationContext(tint).withParams(50.f);
			head.followedBy(blendMode).withParams(ADD);
			return head;
		}

		public void display()
		{
			transformationContext.applyOn(im);
		}
	};

	semaphoreLight = new Drawable()
	{
		private final PImage im = loadImage("imgs/light/semaphore.png");
		private final TransformationContext transformationContext = prepareLightTransformationContext(new PVector(1599, 571));

		public void display()
		{
			if (int(millisWrapper() / 1000) % 2 == 0)
			{
				return;
			}
			transformationContext.applyOn(im);
		}
	};

	bridgeLight = new Drawable()
	{
		private final PImage im = loadImage("imgs/light/bridge.png");
		private final TransformationContext transformationContext = prepareLightTransformationContext(new PVector(862, 589));

		public void display()
		{
			transformationContext.applyOn(im);
		}
	};

	housesLight = new Drawable()
	{
		private final PImage im = loadImage("imgs/light/houses.png");
		private final TransformationContext transformationContext = prepareLightTransformationContext(new PVector(1665, 451));

		public void display()
		{
			transformationContext.applyOn(im);
		}
	};

	outsideLight = new Drawable()
	{
		private final PImage im = loadImage("imgs/light/outside.png");
		private final TransformationContext transformationContext = prepareFireTruckTransformationContext();

		public void display()
		{
			transformationContext.applyOn(im);
		}
	};

	staticSmoke = new StaticSmoke();
	
	topSmoke = new ParticleSystem(new PVector(width * 0.75, 100), new PVector(0, 1), 300, 70, 50);
	vert = new ParticleSystem(new PVector(1452, 757), new PVector(0, -2), 80, 384, 0.1)
		.setAddParticleDeltaFrames(5);
	
	foam = new ParticleSystem(fireman2.pos.copy().add(new PVector(0, 400)), new PVector(-5, 0), 40, 30, 0.5)
		.setColor(color(240));
	
	ovenFire = new Fire(
		new Animation("anim/fire")
			.setRepeat(true)
			.fpsDecimationRatio(2)
			.play()
	).setPosition(new PVector(1400, 591));
	
	drawableSequence = new DrawableSequence();
}

void draw()
{
	if (!loaded)
	{
		final long timePre = System.nanoTime();
		load();
		println("Loading took: " + (System.nanoTime() - timePre) / 1e6d + " millis.");
		loaded = true;
	}
	
	if (playing && tVideoStart == 0)
	{
		tVideoStart = millis();
		if (playMode == PlayMode.WITH_SONG && playing)
		{
			g_song.play(1, songAmplitude);
		}
	}
	
	if (playMode == PlayMode.DEBUG || 
	   (playMode == PlayMode.WITH_SONG && g_song.position() < 85) || 
	   (playMode == PlayMode.EXPORT    && framesToSongPosition() < 85))
	{
		updateState();
	}

	drawableSequence.transformationContext.applyOn(drawableSequence);
	
	knockShakeCanvasTransformationContext.setDiff();
	if (knockShakeCanvasTransformationContext.isApplicable())
	{
		knockShakeCanvasTransformationContext.transformationContext.applyOn(mainCanvasDraw);
	}
	else
	{
		mainCanvasDraw.run();
	}

	initialFAQ.display();
	unstrungHarp.display();
	
	if ((playMode == PlayMode.DEBUG     && animationScale() < 0.73) || 
		(playMode == PlayMode.WITH_SONG && g_song.position() > 85) ||
		(playMode == PlayMode.EXPORT    && framesToSongPosition() > 85))
	{
		//background(255, 240, 190);
		//background(181, 169, 130);
		background(71, 69, 61);
		image(endingFAQ.toDisplay, 0, 0);
	}
	
	if (playMode == PlayMode.EXPORT)
	{
		save("export/" + frameCount + ".png");
	}
	
	// print text
	if (debugInfo)
	{
		textDrawer.printDebugInfo();
	}
	textDrawer.textDrawer.resetCursor();
}

void updateState()
{
	// adjust zoom if possible
	if ((playMode == PlayMode.WITH_SONG && playing && g_song.isPlaying())
		|| (playMode == PlayMode.EXPORT))
	{
		currScale -= (SCALE_PER_SEC / AVG_FPS);
	}
	
	// calculate the object-space-to-screen-space vector
	vectToCorner.set(width - 1920 * currScale, height - 1080 * currScale);
	
	fireTruck.changeState();
	
	car.changeState();

	fireman1.changeState();
	fireman2.changeState();
	
	// handle enter door
	if (timerIsReady(fireTruck.timeStartedToStop, TIME_TO_DOOR + knockManager.getTimeBetweenKnocks(2)))
	{
		enterDoor.open();
	}
	
	if (enterDoor.almostOpened())
	{
		knockShakeCanvasTransformationContext.tShakeStart = millisWrapper();
	}
	
	// handle knocks
	if (timerIsReady(fireTruck.timeStartedToStop, TIME_TO_DOOR))
	{
		knockManager.at(0).activate();
	}
	
	if (timerIsReady(fireTruck.timeStartedToStop, TIME_TO_DOOR + knockManager.getTimeBetweenKnocks()))
	{
		knockManager.at(1).activate();
	}
	
	// control smoke
	if ((playMode == PlayMode.DEBUG     && animationScale() < 0.95) || 
		(playMode == PlayMode.WITH_SONG && g_song.position() > 18) ||
		(playMode == PlayMode.EXPORT    && framesToSongPosition() > 18))
	{
		vert.play();
	}
	if (ovenFire.extinguished)
	{
		vert.pause();
	}
	if (fireTruck.displaying && vert.lightDislocs.size() == 0)
	{
		vert.addLightDisloc(new PVector(10, 0));
		topSmoke.addLightDisloc(new PVector(0, -30));
	}
	if ((playMode == PlayMode.DEBUG     && animationScale() < 0.97) || 
		(playMode == PlayMode.WITH_SONG && g_song.position() > 34) ||
		(playMode == PlayMode.EXPORT    && framesToSongPosition() > 34))
	{
		topSmoke.play();
	}
}

void insertBefore(LinkedList<Drawable> seq, Drawable drawAfter, Drawable drawBefore)
{
	seq.remove(drawBefore);
	seq.add(seq.indexOf(drawAfter), drawBefore);
}

TransformationContext prepareMainCanvasTransformationContext()
{
	final TransformationContext head = new TransformationContext(translate, getGraphics()).withParams(
		new Supplier() 
		{
			private final PVector currentOffset = new PVector();
			public PVector get() 
			{
				currentOffset.set(width - width * currScale, height - height * currScale);
				return currentOffset;
			}
		}
	);
	head.followedBy(scale).withParams(new Supplier() { public Float get() { return relativeScale(); } });
	return head;
}

///////////////////////////////
///////////////////////////////
// Controls
///////////////////////////////
///////////////////////////////

void keyPressed()
{
	if (key == 'B' || key == 'b')
	{
		turnedOffLight = !turnedOffLight;
	}
	else if (key == 'P' || key == 'p' || key == ' ')
	{
		if (playing)
		{
			noLoop();
		}
		else
		{
			loop();
		}
		playing = !playing;
		
		if (g_song == null)
		{
			return;
		}
		if (g_song.isPlaying())
		{
			g_song.pause();
		}
		else
		{
			g_song.play(1, 0.5);
		}
	}
	else if (key == 'k' || key == 'K')
	{
		knockManager.activateNextKnock();
	}
	// back
	else if (keyCode == LEFT)
	{
		rewind(-10, 10 * SCALE_PER_SEC);
	}
	// forth
	else if (keyCode == RIGHT)
	{
		rewind(10, -10 * SCALE_PER_SEC);
	}
	
	if (key == 'O' || key == 'o')
	{
		
		enterDoor.open();
		if (fireExit.opened())
		{
			fireExit.close();
		}
		else
		{
			fireExit.open();
		}
		
		/* playOnButton(animation)
		
		if you want to test out an animation, use playOnButton function
		with the "animation" argument you want to test.
		*/
	}
	
	if (key == 'S' || key == 's')
	{
		car.driving = !car.driving;
	}
	
	if (key == 'R' || key == 'r')
	{
		car.currentPosition = car.START_POSITION.copy();
	}
	
	if (keyCode == UP || keyCode == DOWN)
	{
		if (g_song == null)
		{
			return;
		}
		g_song.amp(constrain(songAmplitude += (keyCode == UP ? amplitudeDiff : -amplitudeDiff), 0.f, 1.f));
	}
}

void mouseWheel(MouseEvent event)
{
	if (currScale <= 1e-3)
	{
		currScale = 1e-3;
	}
	final float amount = event.getCount() * 0.005;
	currScale += amount;
}

void mouseClicked()
{
	redraw();
}

///////////////////////////////
///////////////////////////////
// Aux
///////////////////////////////
///////////////////////////////

float millisSinceFirstDraw()
{
	if (tVideoStart == 0)
	{
		throw new IllegalStateException("This method should not be called sooner than within first draw() execution.");
	}
	return millisWrapper() - tVideoStart;
}

void rewind(float secs, float zoomAmount)
{
	currScale += zoomAmount;
	//currScale = constrain(currScale += zoomAmount, 0.715, 1.0);
	if (g_song == null)
	{
		return;
	}
	float currPos = g_song.position();
	g_song.jump(constrain(currPos += secs, 0.0, g_song.duration()));
}

float relativeScale()
{
	return currScale * SCREEN_WIDTH / 1920f;
}

float animationScale()
{
	return map(currScale, 1.f, INITIAL_SCALE, 0.715f, 1.f);
}

int framesToSongPosition()
{
	return frameCount / 24;
}

float framesToMillis()
{
	return frameCount * (1000.f / (float)AVG_FPS); 
}

float millisWrapper()
{
	return playMode == PlayMode.EXPORT ? framesToMillis() : millis();
}
