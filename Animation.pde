// the UNSET thing can change so that the Animation
// will acquire the position where to draw it from
// Fireman, or whatever else positionable Object.
final static PVector UNSET = new PVector(0, 0);

// Class for animating a sequence of GIFs
class Animation implements Drawable {
	PImage[] images;
	int imageCount;
	int frame;
	private int fpsDecimateRatio = 1;
	boolean playingFwd = true;
	boolean repeat = false;
	boolean playing = false;
	PVector pos = UNSET.copy();
	ArrayList<Animation> attachedAnimations = new ArrayList<Animation>();

	final TransformationContext positionTransformationContext = new TransformationContext(translate).withParams(
		new Supplier()
		{
			public PVector get() { return pos; }
		}
	);
	TransformationContext attachedTransformationContext = new TransformationContext(identity);

	Runnable displayInternal = new Runnable()
	{
		public void run()
		{
			displayInternal();
		}
	};
	
	Animation(String folderPath)
	{
		final File[] allFiles = new File(dataPath("") + File.separator + folderPath).listFiles();
		if (allFiles == null)
		{
			throw new IllegalArgumentException(folderPath + " does not exists.");
		}
		final List<PImage> currImgs = new ArrayList();
		
		for (File f : allFiles)
		{
			if (f.isDirectory())
			{
				continue;
			}
			String filename = folderPath + File.separator + f.getName();
			currImgs.add(loadImage(filename));
		}
		
		images = new PImage[currImgs.size()];
		images = currImgs.toArray(images);
		imageCount = currImgs.size();
	}
	
	Animation(String imagePath, int frameWidth, int leftOffset, boolean readBackwards)
	{
		final PImage fullImage = loadImage(imagePath);
		imageCount = (fullImage.width - leftOffset) / frameWidth;
		images = new PImage[imageCount];
		
		int imageIndex = readBackwards ? imageCount - 1 : 0;
		for (int i = leftOffset; i < fullImage.width; i += frameWidth)
		{
			images[imageIndex] = fullImage.get(i, 0, frameWidth, fullImage.height);
			if (readBackwards)
			{
				--imageIndex;
			}
			else
			{
				++imageIndex;
			}
		}
	}
	Animation(String imagePath, int frameWidth, int leftOffset)
	{
		this(imagePath, frameWidth, leftOffset, false);
	}
	
	Animation(Animation other)
	{
		images = other.images;
		imageCount = other.imageCount;
		frame = 0;
		fpsDecimateRatio = other.fpsDecimateRatio;
		playingFwd = other.playingFwd;
		repeat = other.repeat;
		playing = other.playing;
		pos = other.pos;
		attachedAnimations = new ArrayList<Animation>(other.attachedAnimations);
	}
	
	Animation(String imagePrefix, int count) {
		imageCount = count;
		images = new PImage[imageCount];

		for (int i = 0; i < imageCount; i++) {
			String filename = imagePrefix + i + ".png";
			images[i] = loadImage(filename);
		}
	}
	
	Animation position(PVector pos)
	{
		this.pos = pos;
		return this;
	}
	
	Animation attach(Animation newAnimation)
	{
		attachedAnimations.add(newAnimation);
		return this;
	}

	Animation attach(TransformationContext transformationContext)
	{
		attachedTransformationContext = transformationContext;
		return this;
	}
	
	Animation fpsDecimationRatio(int ratio)
	{
		fpsDecimateRatio = ratio;
		return this;
	}
		
	void display()
	{
		if (pos.equals(UNSET))
		{
			displayInternal();
		}
		else
		{
			positionTransformationContext.applyOn(displayInternal);
		}
		
		if (playing && frameCount % fpsDecimateRatio == 0)
		{
			if (playingFwd)
			{
				incFrame();
			}
			else
			{
				decFrame();
			}
		}
	}
	
	private void displayInternal()
	{
		g_canvas.image(images[frame], 0, 0);
		for (final Animation animation : attachedAnimations)
		{
			animation.attachedTransformationContext.applyOn(animation.images[frame]);
			if (playing && frameCount % fpsDecimateRatio == 0)
			{
				if (playingFwd)
				{
					animation.incFrame();
				}
				else
				{
					animation.decFrame();
				}
			}
		}
	}

	private void incFrame()
	{
		if (frame == imageCount - 1 && !repeat)
		{
			frame = imageCount - 1;
		}
		else
		{
			frame = ++frame % imageCount;
		}
	}
	
	private void decFrame()
	{
		if (frame == 0 && !repeat)
		{
			frame = 0;
		}
		else if (--frame == -1)
		{
			frame = imageCount - 1;
		}
	}
	
	void playFwd()
	{
		frame = 0;
		playingFwd = true;
	}
	
	void playBwd()
	{
		frame = imageCount - 1;
		playingFwd = false;
	}
	
	boolean isPlaying()
	{
		return playing;
	}
	
	Animation play()
	{
		playing = true;
		return this;
	}
	
	void pause()
	{
		playing = false;
	}
	
	Animation setRepeat(boolean rep)
	{
		repeat = rep;
		return this;
	}
	
	boolean ended()
	{
		return isPlayingFwd() ? frame == imageCount - 1 : frame == 0;
	}
	
	boolean isPlayingFwd()
	{
		return playingFwd;
	}
	
	int getWidth() {
		return images[0].width;
	}
	
	PImage getCurrentFrame()
	{
		return images[frame];
	}
}

void playOnButton(final Animation animation)
{
	if (!animation.isPlaying())
	{
		animation.play();
	}

	if (animation.isPlayingFwd())
	{
		animation.playBwd();
	}
	else
	{
		animation.playFwd();
	}
}
