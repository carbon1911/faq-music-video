import os
import cv2
import glob

RELATIVE_EXPORTED_IMGS_DIRECTORY = "../export_tmp/"

def numerical_sort(value: str) -> int:
    return int("".join(filter(str.isnumeric, os.path.splitext(value)[0])))

if __name__ == "__main__":
    videoWriter = cv2.VideoWriter("export.avi", cv2.VideoWriter_fourcc(*'DIVX'), 24, (1920, 1080))
    for filename in sorted(glob.glob(f"{RELATIVE_EXPORTED_IMGS_DIRECTORY}*.png"), key=numerical_sort):
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width, height)

        videoWriter.write(img)
        
        print(f"Processing image {filename}")

    videoWriter.release()
    