import moviepy.editor as mpe

if __name__ == "__main__":
    video_file = mpe.VideoFileClip("export.avi")
    merged = video_file.set_audio(mpe.AudioFileClip("../data/faq.mp3"))
    merged.write_videofile("export_with_sound.mp4", codec="png", threads=6)

    video_file.close()
    merged.close()