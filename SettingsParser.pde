import java.io.FileReader;

/**
    This guy parses arguments using parse method. The method needs as an argument
    a path to a config file relative to sketchPath("") path. The method returns a
    map of string, string values. It is up to the user of this class to parse 
    the returnedvalues in an adequate manner.
*/
class SettingsParser
{
    Map<String, String> parse(String configFilePath)
    {
        Map<String, String> settings = new HashMap<String, String>();
        
        BufferedReader br = null;
        try
        {
            String path = sketchPath("") + File.separator + configFilePath;
            br = new BufferedReader(new FileReader(path));
            String line;
            String[] lineParts;
            while ((line = br.readLine()) != null)
            {
                if (line.startsWith("#"))
                {
                    continue;
                }
                lineParts = line.split("=");
                settings.put(lineParts[0], lineParts[1]);
            }
            br.close();
        }
        catch (IOException ioe)
        {
            System.err.println("An error occurred creating a FileReader object from " + configFilePath + " file.");
        }
        return settings;
    }
}
