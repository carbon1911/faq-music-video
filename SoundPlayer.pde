class SoundPlayer
{
    SoundFile sound = null;
    float amplitude = 0.5f;
    float amplitudeStep = 0.1f;
    boolean paused = false;
    
    SoundPlayer(SoundFile soundFile)
    {
        sound = soundFile;
    }
    
    void increaseVolume()
    {
        changeVolume(amplitudeStep);
    }
    
    void decreaseVolume()
    {
        changeVolume(-amplitudeStep);
    }
    
    void changeVolume(float amount)
    {
        sound.amp(constrain(amplitude += amount, 0.f, 1.f));
    }
    
    void rewind(float amount)
    {
        final float currPos = sound.position();
        sound.jump(constrain(currPos + amount, 0.0, sound.duration()));
    }
}
