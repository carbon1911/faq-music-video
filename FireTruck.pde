class FireTruck implements Drawable, ConditionallyDisplayable
{
	Siren siren = new Siren();
	Driver driver = new Driver();
	Door frontDoor = new Door(new Animation("data/anim/fire truck door.png", 76, 0));
	Animation firemanEnteringFireTruck = new Animation("data/anim/fireman/entering fire truck.png", 400, 0)
		.fpsDecimationRatio(ANIMATION_FPS_DECIMATE_FACTOR);
	Animation fireman2EnteringFireTruck = new Animation(firemanEnteringFireTruck);
	PImage imageFireTruck = loadImage("imgs/fire truck.png");
	
	PVector position = new PVector(296, 600);
	final float INITIAL_SPEED = 20.f;
	final float ACCELERATION = 0.43f;
	float speed = INITIAL_SPEED;
		
	boolean frontDoorClosed = false;
	boolean displaying = false;
	
	float timeStartedToStop = 0.f;
	
	final static float START_SLOW_DOWN_POSITION = 950.f;

	TransformationContext fireman1TransformationContext = null;
	TransformationContext fireman2TransformationContext = null;

	TransformationContext positionTransformationContext = null;
    Runnable displayContext = new Runnable()
    {
        public void run()
        {
            displayContext();
        }
    };

	TransformationContext frontDoorTransformationContext = null;
	
	FireTruck()
	{
		final TransformationContext fireman1TransformationContextHead = new TransformationContext(translate);
		fireman1TransformationContextHead.withParams(300.f, 0.f)
			.followedBy(scale).withParams(new Supplier()
			{
				Float get() { return displayWalkingBack.smallSize(fireman1); }
			});
		fireman1TransformationContext = fireman1TransformationContextHead;

		final TransformationContext fireman2TransformationContextHead = new TransformationContext(translate);
		fireman2TransformationContextHead.withParams(300.f, 0.f)
			.followedBy(scale).withParams(new Supplier()
			{
				Float get() { return displayWalkingBack.smallSize(fireman2); }
			});
		fireman2TransformationContext = fireman2TransformationContextHead;

		positionTransformationContext = new TransformationContext(translate)
			.withParams(new Supplier() { PVector get() { return fireTruck.position; } });

		frontDoorTransformationContext = new TransformationContext(translate).withParams(320.f, 17.f);
	}

	void display()
	{
		if (!(displaying = 
		(playMode == PlayMode.DEBUG && animationScale() < 0.91) || 
		(playMode == PlayMode.WITH_SONG && g_song.position() > 32) ||
		(playMode == PlayMode.EXPORT && framesToSongPosition() > 32)))
		{
			return;
		}
		
		positionTransformationContext.applyOn(displayContext);
	}
	
	private void displayContext()
	{
		if (fireman2.state == FiremanState.ENTERING_FIRE_TRUCK && !fireman2EnteringFireTruck.ended())
		{
			fireman2EnteringFireTruck.play();
			fireman2TransformationContext.applyOn(fireman2EnteringFireTruck);
		}
		
		driver.display();
		g_canvas.image(imageFireTruck, 0, 0);
		
		if (fireman1.state == FiremanState.ENTERING_FIRE_TRUCK && !firemanEnteringFireTruck.ended())
		{
			firemanEnteringFireTruck.play();
			fireman1TransformationContext.applyOn(firemanEnteringFireTruck);
		}
		
		if (firemanEnteringFireTruck.ended())
		{
			frontDoorClosed = frontDoor.anim.frame == 0;
			frontDoor.close();
		}
		frontDoorTransformationContext.applyOn(frontDoor);
		
		siren.display();
	}

	boolean isDisplayable()
	{
		return displaying;
	}
	
	float absoluteDistanceFromStopPlaceClamped()
	{
		final float distance = map(constrain(abs(START_SLOW_DOWN_POSITION + 200 - fireTruck.position.x), 0.f, 1000.f), 1000.f, 0.f, 0.f, 1.f);
		return distance * distance;
	}
	
	boolean hasStopped()
	{
		return speed > 0.1f;
	}
	
	void changeState()
	{
		if (!displaying)
		{
			return;
		}
		// tbh I've lost track what's going on here long time ago. I'm glad that it works. Sorry for an ugly piece of code.
		if (!frontDoorClosed && position.x > START_SLOW_DOWN_POSITION && position.x > 0.f && speed > 0.f)
		{
			speed -= ACCELERATION;
		}
		if (frontDoorClosed && speed <= INITIAL_SPEED && fireman2EnteringFireTruck.ended())
		{
			speed += ACCELERATION;
		}
		
		if (hasStopped())
		{
			position.x += speed;
		}
		else if (timeStartedToStop < 0.1f)
		{
			timeStartedToStop = millisWrapper();
		}
	}
}

class Siren implements Drawable
{
	final PImage light = loadImage("imgs/siren.png");
	final TransformationContext transformationContext = prepareTransformationContext();

	private TransformationContext prepareTransformationContext()
	{
		final TransformationContext head = new TransformationContext(translate)
			.withParams(-150.f, -170.f);
		head.followedBy(prepareFireTruckTransformationContext());
		return head;
	}

	void display()
	{
		transformationContext.applyOn(light);
	}
	
	float visibility()
	{
		return map(cos(millisWrapper() / 125.f), -1, 1, 0, 255);
	}
}

class Driver implements Drawable
{
	PImage imageDriver = loadImage("imgs/driver.png");
	
	void display()
	{
		if (isDisplaying())
		{
			g_canvas.image(imageDriver, 0, 0);
		}
	}
	
	boolean isDisplaying()
	{
		return fireTruck.speed >= 0.1f || fireTruck.firemanEnteringFireTruck.ended() || fireTruck.fireman2EnteringFireTruck.ended();
	}
}
