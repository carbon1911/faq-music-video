TransformationContext prepareFireTruckTransformationContext()
{
	final TransformationContext head = new TransformationContext(blendMode).withParams(ADD);
	head.followedBy(tint).withParams(new Supplier()
	{
		public Float get() 
		{
			return fireTruck.siren.visibility() * fireTruck.absoluteDistanceFromStopPlaceClamped();
		}
	});
	return head;
}

TransformationContext prepareLightTransformationContext(final PVector position)
{
	final TransformationContext head = new TransformationContext(translate).withParams(position);
	head.followedBy(blendMode).withParams(ADD);
	return head;
}

// TODO: this file might be unnecessary. Single Light class should be enough.
// Light itself is just an image, drawn at certain position with ADD blend mode,
// so maybe even Light class itself is redundant. 
abstract class Light implements Drawable
{
	PVector pos = null;
	PImage im = null;

	Light(PImage lightImage, PVector position)
	{
		im = lightImage;
		this.pos = position;
	}
}

abstract class ConditionalLight extends Light implements ConditionallyDisplayable
{
	ConditionalLight(PImage lightImage, PVector position)
	{
		super(lightImage, position);
	}
	
	abstract void displayLight();
	void display()
	{
		if (!isDisplayable())
		{
			return;
		}
		displayLight();
	}
}

abstract class RoomLight extends ConditionalLight
{
	private final TransformationContext transformationContext = prepareTransformationContext();
	RoomLight(PImage lightImage, PVector pos)
	{
		super(lightImage, pos);
	}
	
	void displayLight()
	{
		transformationContext.applyOn(im);
	}

	private TransformationContext prepareTransformationContext()
	{
		final TransformationContext head = new TransformationContext(translate)
			.withParams(new Supplier() { public PVector get() { return pos; } });
		head.followedBy(prepareFireTruckTransformationContext());
		return head;
	}
}

class OpenDoorLight extends RoomLight
{
	OpenDoorLight(PImage lightImage, PVector pos)
	{
		super(lightImage, pos);
	}
	
	boolean isDisplayable()
	{
		return enterDoor.anim.ended();
	}
}

class CeilingLight extends ConditionalLight
{
	final PVector X_LINE = new PVector(1, 0);
	float currShear = 0.0;
	final PImage mask = loadImage("imgs/ceiling light mask.png");
	PGraphics buffer = null;
	
	TransformationContext lightBufferTransformationContext = null;
	TransformationContext mainCanvasTransformationContext = null;
	
	CeilingLight(PImage lightImage)
	{
		super(lightImage, new PVector(763, 463));

		buffer = createGraphics(im.width, im.height, P2D);

		mainCanvasTransformationContext = prepareMainCanvasTransformationContext();
		lightBufferTransformationContext = prepareLightBufferTransformationContext();
	}
	
	boolean isDisplayable()
	{
		//return fireTruck.displaying;
		return true;
	}
	
	void displayLight()
	{
		final PVector lightTop = vectToCorner.copy().add(pos.copy().add(im.width / 2, 0)).mult(currScale);
		final PVector fireTruckMiddle = vectToCorner.copy().add(fireTruck.position.copy()).mult(currScale);
		final PVector res = fireTruckMiddle.sub(lightTop);
		currShear = res.dot(X_LINE) / res.mag();
		
		lightBufferTransformationContext.applyOn(im);
		
		if (!fireTruck.displaying)
		{
			return;
		}
		mainCanvasTransformationContext.applyOn(buffer);
	}

	private TransformationContext prepareMainCanvasTransformationContext()
	{
		final TransformationContext head = new TransformationContext(translate)
			.withParams(new Supplier() { public PVector get() { return pos; } });
		head.followedBy(prepareFireTruckTransformationContext());
		return head;
	}

	private TransformationContext prepareLightBufferTransformationContext()
	{
		final TransformationContext bufferHead = new TransformationContext(endBeginDraw).withParams(g_canvas);
		bufferHead.followedBy(beginEndDraw).withParams(buffer)

		// custom pre/post transformation
		.followedBy(new Transformation()
		{
			public void pre(PGraphics canvas) { canvas.clear(); }
			public void post(PGraphics canvas) { canvas.mask(mask); }
		})
		.followedBy(translate).withParams(new PVector(0, im.height))
		.followedBy(shearX).withParams(new Supplier(){ public Float get() { return currShear; } })
		.followedBy(scale).withParams(1.f, -1.f);
		return bufferHead;
	}
}
