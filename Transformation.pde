///////////////////////////////
///////////////////////////////
// Transformation
///////////////////////////////
///////////////////////////////

abstract class Transformation
{
	void pre(PGraphics canvas) {throw new UnsupportedOperationException();}
	void pre(PGraphics canvas, Object[] params) 
	{
		for (StackTraceElement ste : Thread.currentThread().getStackTrace())
		{
			System.err.println(ste.toString());
		}
		throw new UnsupportedOperationException(params[0].getClass().getSimpleName());
	}

	abstract void post(PGraphics canvas);
}

class TransformationContext
{
	Transformation t = null;
	TransformationContext n = null;
	Supplier supplier = null;
	Object[] params = null;
	PGraphics currentCanvas = g_canvas;

	TransformationContext(Transformation t)
	{
		this.t = t;
	}

	// Assumes canvas is in the state eager for drawing, e.g., beginDraw has been called etc.
	TransformationContext(Transformation t, PGraphics canvas)
	{
		this.t = t;
		this.currentCanvas = canvas;
	}

	TransformationContext followedBy(TransformationContext n)
	{
		this.n = n;
		n.currentCanvas = currentCanvas;
		return n;
	}

	TransformationContext followedBy(Transformation t)
	{
		this.n = new TransformationContext(t, this.currentCanvas);
		return this.n;
	}

	TransformationContext withParams(Object... params)
	{
		if (supplier != null)
		{
			throw new IllegalStateException("Supplier has already been supplied. Cannot supply more arguments.");
		}
		this.params = params;
		return this;
	}

	TransformationContext withParams(final Supplier supplier)
	{
		if (params != null)
		{
			throw new IllegalStateException("Object[] parameters have already been supplied. Cannot use Supplier anymore.");
		}
		this.supplier = supplier;
		return this;
	}

	// Assumes canvas is in the state eager for drawing, e.g., beginDraw has been called etc.
	TransformationContext withParams(final PGraphics canvas)
	{
		this.currentCanvas = canvas;
		return this;
	}

	TransformationContext getTail()
	{
		TransformationContext current = this;
		while (current.n != null)
		{
			current = n;
		}
		return current;
	}

	public void applyOn(PImage s)
	{
		// pre
		pre();
		
		// action
		if (n == null)
		{
			currentCanvas.image(s, 0, 0);
		}
		else
		{
			n.applyOn(s);
		}

		// post
		t.post(currentCanvas);
	}

	public void applyOn(Drawable s)
	{
		// pre
		pre();
		
		// action
		if (n == null)
		{
			s.display();
		}
		else
		{
			n.applyOn(s);
		}

		// post
		t.post(currentCanvas);
	}

	public void applyOn(Runnable r)
	{
		// pre
		pre();
		
		// action
		if (n == null)
		{
			r.run();
		}
		else
		{
			n.applyOn(r);
		}

		// post
		t.post(currentCanvas);
	}

	public <T> void applyOn(Consumer c, T argument)
	{
		// pre
		pre();
		
		// action
		if (n == null)
		{
			c.accept(argument);
		}
		else
		{
			n.applyOn(c, argument);
		}

		// post
		t.post(currentCanvas);
	}

	public void applyOn(MyCallable c, Fireman fireman, Object... args)
	{
		// pre
		pre();
		
		// action
		if (n == null)
		{
			c.call(fireman, args);
		}
		else
		{
			n.applyOn(c, fireman, args);
		}

		// post
		t.post(currentCanvas);
	}

	private void pre()
	{
		if (t == null)
		{
			println("t is null");
		}
		if (currentCanvas == null)
		{
			println("current canvas is null");
		}
		
		if (supplier != null)
		{
			t.pre(currentCanvas, new Object[]{ supplier.get() });
		}
		else if (params != null)
		{
			t.pre(currentCanvas, params);
		}
		else
		{
			t.pre(currentCanvas);
		}
	}
}

///////////////////////////////
///////////////////////////////
// Actual transformations
///////////////////////////////
///////////////////////////////

final Transformation identity = new Transformation()
{
	void pre(PGraphics canvas) {}
	void post(PGraphics canvas) {}
};

final Transformation translate = new Transformation()
{
	void pre(PGraphics canvas, Object[] arguments)
	{
		canvas.pushMatrix();
		switch (arguments.length)
		{
			case 1:
				final PVector vector = (PVector)arguments[0];
				canvas.translate(vector.x, vector.y);
				break;
			case 2:
				canvas.translate((float)arguments[0], (float)arguments[1]);
				break;
			default:
				throw new IllegalArgumentException();
		}
	}

	void post(PGraphics canvas)
	{
		canvas.popMatrix();
	}
};

final Transformation rotate = new Transformation()
{
	void pre(PGraphics canvas, Object[] arguments)
	{
		canvas.pushMatrix();
		canvas.rotate((float)arguments[0]);
	}

	void post(PGraphics canvas)
	{
		canvas.popMatrix();
	}
};

final Transformation imageMode = new Transformation()
{
	void pre(PGraphics canvas, Object[] arguments)
	{
		canvas.pushStyle(); canvas.imageMode((Integer)arguments[0]);
	}
	
	void post(PGraphics canvas)
	{
		canvas.popStyle();
	}
};

final Transformation transparency = new Transformation()
{
	void pre(PGraphics activeCanvas, Object[] arguments)
	{
		activeCanvas.pushStyle();
		
		// according to documentation: https://processing.org/reference/tint_.html,
		// int -> RGB, float -> bnw, support RGB for now
		activeCanvas.tint(255, (int)arguments[0]);
	}

	void post(PGraphics activeCanvas)
	{
		activeCanvas.popStyle();
	}
};

final Transformation tint = new Transformation()
{
	void pre(PGraphics activeCanvas, Object[] arguments)
	{
		activeCanvas.pushStyle();
		try
		{
			activeCanvas.tint((float)arguments[0]);
		}
		catch (ClassCastException e)
		{
			try 
			{
				final float[] args = (float[])arguments[0];
				activeCanvas.tint((float)args[0], (float)args[1], (float)args[2], (float)args[3]);
			}
			catch (ClassCastException e1)
			{
				throw e1;
			}
		}
	}

	void post(PGraphics canvas)
	{
		canvas.popStyle();
	}
};

Transformation scale = new Transformation()
{
	void pre(PGraphics canvas, Object[] arguments)
	{
		canvas.pushMatrix();
		switch (arguments.length)
		{
			case 1:
				canvas.scale((float)arguments[0]);
				break;
			case 2:
				canvas.scale((float)arguments[0], (float)arguments[1]);
				break;
			default:
				throw new IllegalArgumentException();
		}
	}

	void post(PGraphics canvas)
	{
		canvas.popMatrix();
	}
};

final Transformation blendMode = new Transformation()
{
	public void pre(PGraphics canvas, Object[] arguments)
	{
		canvas.blendMode((int)arguments[0]);
	}

	public void post(PGraphics canvas)
	{
		canvas.blendMode(BLEND);
	}
};

final Transformation shearX = new Transformation()
{
	public void pre(PGraphics canvas, Object[] params)
	{
		canvas.pushMatrix();
		canvas.shearX((float)params[0]);
	}

	public void post(PGraphics canvas)
	{
		canvas.popMatrix();
	}
};

final Transformation ellipseMode = new Transformation()
{
	public void pre(PGraphics canvas, Object[] params)
	{
		canvas.ellipseMode((int)params[0]);
	}

	public void post(PGraphics canvas)
	{
		canvas.ellipseMode(CORNER);
	}
};

final Transformation stroke = new Transformation()
{
	public void pre(PGraphics canvas, Object[] params)
	{
		canvas.pushStyle();
		if (params.length == 3)
		{
			canvas.stroke((float)params[0], (float)params[1], (float)params[2]);
		}
		else
		{
			throw new UnsupportedOperationException();
		}
	}

	public void post(PGraphics canvas)
	{
		canvas.popStyle();
	}
};

final Transformation noStroke = new Transformation()
{
	public void pre(PGraphics canvas)
	{
		canvas.pushStyle();
		canvas.noStroke();
	}

	public void post(PGraphics canvas)
	{
		canvas.popStyle();
	}
};

final Transformation fill = new Transformation()
{
	public void pre(PGraphics canvas, Object[] params)
	{
		canvas.pushStyle();
		canvas.fill((color)params[0]);
	}

	public void post(PGraphics canvas)
	{
		canvas.popStyle();
	}
};

final Transformation beginEndDraw = new Transformation()
{
	public void pre(PGraphics canvas)
	{
		canvas.beginDraw();
	}

	public void post(PGraphics canvas)
	{
		canvas.endDraw();
	}
};

final Transformation endBeginDraw = new Transformation()
{
	public void pre(PGraphics canvas)
	{
		canvas.endDraw();
	}

	public void post(PGraphics canvas)
	{
		canvas.beginDraw();
	}
};

final Transformation clear = new Transformation()
{
	public void pre(PGraphics canvas)
	{
		canvas.clear();
	}

	public void post(PGraphics canvas) {}
};
