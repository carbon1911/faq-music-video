class Earphone implements Drawable
{
    Earphone()
	{
		final TransformationContext head = new TransformationContext(translate);
		head.withParams(position)
			.followedBy(new TransformationContext(rotate)).withParams(
                new Supplier()
                { 
                    Float get() 
                    { 
                        return PI/8.0 * cos(map(millisWrapper() % 3000, 0, 3000, 0, TWO_PI)); 
                    }
                })
			.followedBy(new TransformationContext(translate)).withParams(new PVector(0, image.height / 2))
			.followedBy(new TransformationContext(imageMode)).withParams(CENTER);
		tc = head;
	}

	final PVector position = new PVector(98, 545);
	final PImage image = loadImage("imgs/earphone.png");
    TransformationContext tc = null;

	void display()
	{
		if (animationScale() <= 0.8)
		{
			tc.applyOn(image);
		}
	}
}
